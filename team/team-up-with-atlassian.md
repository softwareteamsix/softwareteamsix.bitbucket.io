---
title: Team up with Atlassian
platform: cloud
product: integrate
category: team
subcategory: teams
date: "2017-02-16"
---
# Team up with Atlassian

Let's get your team up and running! You've already decide that you want to use JIRA Software and Bitbucket. That's the first step.

Before you jump into your agile flow and brilliant coding, you've got some setup tasks. This page should get most of that stuff out of the way, making you ready to tackle the important things.

<table class='aui'>
    <tr>
        <td>image goes here</td>
        <td>
          <h2>Get JIRA Software</h2>

          When you get JIRA Software, you can start with a 7-day trial before you start paying. When choosing a trial version, we recommend you also get Confluence because you can use it to record requirements for your team's projects. <a href="https://www.atlassian.com/software/jira/try" target="_blank">Get trial</a>

          Once you've got JIRA Software (and possibly Confluence), you'll be able to select the User management option from the top-right dropdown to manage all your users for both products. This part of the site is also where you'd claim your email domain. We recommend you claim your email domain so that you have more control over your users' accounts and so that you can, most importantly, set authentication policies. <a href="https://confluence.atlassian.com/x/gjcWN" target="_blank">Learn more</a>
        </td>
    </tr>
    <tr>
      <td> image goes here</td>
      <td>
        <h2>Invite users to your site </h2> 

        Your users will log in to JIRA and Bitbucket with an Atlassian account. To add users to JIRA, you'll enter their email addresses. If an email address matches an existing Atlassian account, that user is able to use their account to log in to JIRA automatically. If an email address doesn't match an existing account, the user receives a link in an email invitation that walks them through creating an Atlassian account. After they create their account, they'll be able to log in to your JIRA site. <a href="https://confluence.atlassian.com/x/2IxjL" target="_blank">Learn more</a>

        When you add their email addresses to your site, make you JIRA Software (and any other products you want them to be able to access) have checkmarks.
      </td>
    </tr> 
    <tr>
        <td> Image goes here</td>
        <td><h2>Set up a JIRA Software project</h2>

            Now that your Atlassian Cloud site is up and running, and you've also invited users to your site, it's time to get your team working and shipping. The first step is to set up a JIRA Software project. Agile teams typically work in either <a href="<a href="https://confluence.atlassian.com/x/2IxjL" target="_blank>Scrum</a> or <a href="<a href="https://www.atlassian.com/agile/kanban" target="_blank">Learn more</a>" target="_blank">Kanban</a> projects, but if you want to familiarize yourself with the tool first, you can create a sample project for now. <a href="<a href="https://confluence.atlassian.com/x/-AGRLQ" target="_blank">Learn more</a>" target="_blank">Learn more</a>

            Either way, your new project comes with a new board, depending on the project type that you selected. <a href="<a href="https://confluence.atlassian.com/x/-QGRLQ" target="_blank">Learn more</a>" target="_blank">Learn more</a>

            Once your project is set up, you'll need to invite users to your project. Before you do, make sure that the users you want to invite are already added to your Atlassian Cloud site. <a href="<a href="https://confluence.atlassian.com/x/oINWLw" target="_blank">Learn more</a>" target="_blank">Learn more</a>

            Just a few things to note when inviting users:

            <ul>
                <li>Invitees can only create an account under the email address the invitation was sent to.</li>
                <li>Your user license count will not be affected until invitees accept the invitation and the users are created. </li>
                <li>Each invitation will expire seven days after it was sent.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>image goes here</td>
        <td>
            <h2>Set up a repository in Bitbucket</h2>
            A repository is the place where you keep all your code. Unless you're starting from scratch, you've probably already been working on some code. Once you've got the empty repository in Bitbucket, you can get that code onto your Bitbucket repository. If you don't have any code, you'll create a repository that'll remain empty for now. <a href="https://confluence.atlassian.com/x/WqW5Lw" target="_blank">Learn more</a>

            As you've worked on and saved your code, have you been using Git?

            <ul>
                <li>If so, you just need to push that code up to Bitbucket with a few simple Git commands. <a href="https://confluence.atlassian.com/x/OalIN" target="_blank">Learn more</a> </li>
                <li>If you haven't been versioning your code, we recommend that you version it in Git. That way, you can then use Git to push that code to your empty repository. <a href="https://confluence.atlassian.com/x/LalIN" target="_blank">Learn more</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>image goes here </td>
        <td>
            <h2>Integrate JIRA Software with Bitbucket</h2>

            When you connect JIRA Software and Bitbucket, you can:

            <ul>
                <li>Integrate issues and code seamlessly when you include the issue key in a branch name, commit message, or even the title of a pull request.</li>
                <li>Stay within the context of either JIRA Software and Bitbucket, and still see the work that's going on on both applications.</li>
                <li>Sync pull requests, commits, and branches to your JIRA Software workflow, so that issues 
            </ul>
            
            To connect JIRA Software with Bitbucket, you'll need to:

            <ol>
                <li>Log into both JIRA Software and the Bitbucket account that has the repos you want to connect to JIRA Software</li>
                <li>Grant access for JIRA to your Bitbucket account</li>
                <li>Grant Bitbucket access to JIRA</li>
            </ol>

            <a href="https://confluence.atlassian.com/x/jIeHM" target="_blank">Learn more</a> about integrating JIRA Software with Bitbucket.

            With both applications integrated, you can make work more seamless for your team by enabling Smart Commits in JIRA Software. With Smart Commits enabled, you can use specific commands in your commit messages in Bitbucket — transition, for example — and this triggers an automatic action in JIRA Software. In this case, the transition command automatically moves a JIRA Software issue to a particular workflow state. <a href="https://confluence.atlassian.com/x/RH1NLg" target="_blank">Learn more</a>
        </td>
    </tr>
</table>

You're all set up and ready to go! Remember that working on a team is challenging and rewarding at the same time. Now that you've got the software tools to get you going, check out our [Atlassian Team Playbook](https://www.atlassian.com/team-playbook). These plays will give your some suggestions and structure for working a team. Take what works and leave what doesn't. To get started, we highly recommend the [Health Monitor](https://www.atlassian.com/team-playbook/health-monitor), which get the discussion going to make sure everyone is on the same page and prevent surprises at inopportune moments.
