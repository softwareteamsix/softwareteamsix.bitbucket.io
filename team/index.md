---
title: Team up with Atlassian
platform: cloud
product: integrate
category: team
subcategory: index
date: "2017-02-16"
---
{{< reuse-page path="content/cloud/integrate/team/team-up-with-atlassian.md">}}
