---
title: Integrated software development with Atlassian
platform: cloud
product: integrate

date: "2016-01-09"
---
# Integrated software development with Atlassian

![Atlassian Application Development Lifecycle](../integrate/images/atlassian-adl.png "Atlassian Application Development Lifecycle")

