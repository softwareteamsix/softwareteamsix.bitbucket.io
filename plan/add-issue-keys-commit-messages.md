---
title: Always add issue keys to Git commit messages
platform: cloud
product: integrate
category: plan
subcategory: track

date: "2017-01-09"
---
# Always add issue keys to Git commit messages
