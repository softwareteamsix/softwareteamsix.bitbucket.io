---
title: Transition issues when you push to Bitbucket
platform: cloud
product: integrate
category: plan
subcategory: track

date: "2017-01-09"
---
# Transition issues when you push to Bitbucket
