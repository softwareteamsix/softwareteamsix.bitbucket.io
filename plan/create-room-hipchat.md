---
title: Create a room in HipChat
platform: cloud
product: integrate
category: plan
subcategory: integration

date: "2017-01-23"
---
# Create a room in HipChat
