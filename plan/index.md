---
title: Plan and manage projects with Atlassian
platform: cloud
product: integrate
category: plan
subcategory: index
date: "2017-01-09"
---
# Plan and manage projects with Atlassian

Welcome to JIRA Software Cloud development! This overview will cover everything you need to know to integrate with JIRA Cloud. This includes the Atlassian Connect framework, which is used to integrate with Atlassian Cloud applications, as well as JIRA Software Cloud features and services that you can use when building an add-on.

{{% tip title="Hello world"%}}If you already know the theory and want to jump straight into development, read our [Getting started guide](/cloud/jira/software/getting-started) to build your first JIRA Cloud add-on.{{% /tip %}}

<br>
We recommend that you read this entire page if you haven't integrated with JIRA Software Cloud before, but if you want to jump to a specific section, use the links below:

-   [What is JIRA Software?](#overview)
-   [JIRA Software Cloud and Atlassian Connect](#jira-software-cloud-and-atlassian-connect)
-   [Building blocks for integrating with JIRA Software Cloud](#building-blocks-for-integrating-with-jira-software-cloud) *(REST API, webhooks, and modules)*
-   [JIRA Software Cloud and the JIRA platform](#jira-software-cloud-and-the-jira-platform)
-   [Looking for inspiration?](#inspiration)
-   [More information](#more-information)

## What is JIRA Software Cloud? <a name="overview"></a>

JIRA Software Cloud helps software teams plan, track, and release great software. It's a hosted service from Atlassian for agile software development, with features sprint planning, scrum and kanban boards, agile reports, dashboards, and more. You can also integrate JIRA Software Cloud with source code management and continuous integration tools to streamline your development processes.

Thinking about building an add-on or integration for JIRA Software? JIRA Software is already used by thousands of companies, like NASA, Spotify, and Twitter; and there are over a thousand JIRA-related add-ons in the Atlassian marketplace. The opportunities for extending JIRA Software are endless!

If you haven't used JIRA Software before, check out the [product overview] for more information.

## JIRA Software Cloud and Atlassian Connect

If you want to integrate with any JIRA Cloud product, including JIRA Software Cloud, then you should use Atlassian Connect. Atlassian Connect is an extensibility framework that handles discovery, installation, authentication, and seamless integration into the JIRA UI. An Atlassian Connect add-on could be an integration with another existing service, new features for JIRA, or even a new product that runs within JIRA.

If you haven't used Atlassian Connect before, check out the [Getting started guide]. This guide will help you learn how to set up a development environment and build a JIRA Cloud add-on.

## Building blocks for integrating with JIRA Software Cloud

The three building blocks of integrating with JIRA Software are the REST API, web hooks, and web fragments.

### JIRA Software Cloud REST API

|              |                 |
|--------------|-----------------|
| ![](../../../illustrations/atlassian-software-47.png) | The JIRA Software Cloud REST API lets your add-on communicate with JIRA Software Cloud. For example, using the REST API, you can write a script that moves issues from one sprint to another.<br>See the [JIRA Software Cloud REST API] reference for details.<br>*Note, JIRA Software is built on the JIRA platform, so you can also use the [JIRA Cloud platform REST API] to interact with JIRA Software Cloud.* |

### Webhooks

|              |                 |
|--------------|-----------------|
| ![](../../../illustrations/atlassian-software-46.png) | Webhooks notify your add-on or application when certain events occur in JIRA Software. For example, you could create a webhook that alerts your application when a sprint has ended. JIRA Software allows you to register webhooks for events related to sprints, boards, and more.<br> For more information, see [Webhooks].|

### JIRA Software modules

|              |                 |
|--------------|-----------------|
| ![](../../../illustrations/atlassian-software-52.png) | A module is simply a UI element, like a tab or a menu. JIRA Software UI modules allow add-ons to interact with the JIRA Software UI. For example, your add-on can use a JIRA Software UI module to add a new dropdown menu to a board.<br>For more information, see [About JIRA modules].|

## JIRA Software Cloud and the JIRA platform

JIRA Software is an application built on the JIRA platform. The JIRA platform provides a set of base functionality that is shared across all JIRA applications, like issues, workflows, search, email, and more. A JIRA application is an extension of the JIRA platform that provides specific functionality. For example, JIRA Software adds boards, reports, development tool integration, and other agile software development features.

This means that when you develop for JIRA Software, you are actually integrating with the JIRA Software application as well as the JIRA platform. The JIRA Software application and JIRA platform have their own REST APIs, web hook events, and web fragments.

Read the overview of the [JIRA Cloud platform documentation] for more information.

## Looking for inspiration? <a name="inspiration"></a>

If you are looking for ideas on building the next JIRA Software Cloud integration, here are a few examples of what you can build on top of JIRA Software Cloud:

-   [Retrospective Tools for JIRA] -- This add-on provides a number of tools for reviewing your project history, and with the JIRA Software API they're able to let you filter by board, or even overlay your swimlanes.
-   [Epic Sum Up] -- This add-on adds a panel to the view issue page that allows you to review your epic progress or every issue within the epic.
-   [Tempo Planner for JIRA] -- This add-on fetches information from boards, epics, and backlogs using the JIRA Software API, and lets you plan your work by team member.

## More information

-   [JIRA Software Cloud tutorials] -- Learn more about JIRA Software development by trying one of our hands-on tutorials.
-   [jira-agile-development tag] on the Atlassian Answers forum -- Join the discussion on JIRA Software development.

  [product overview]: https://www.atlassian.com/software/jira
  [Getting started guide]: /cloud/jira/software/getting-started
  [JIRA Software Cloud REST API]: https://docs.atlassian.com/jira-software/REST/cloud/
  [JIRA Cloud platform REST API]: https://docs.atlassian.com/jira/REST/cloud/
  [Webhooks]: /cloud/jira/software/webhooks
  [About JIRA modules]: /cloud/jira/software/about-jira-modules
  [JIRA Cloud platform documentation]: /cloud/jira/platform/integrating-with-jira-cloud
  [Retrospective Tools for JIRA]: https://marketplace.atlassian.com/plugins/com.sngtec.jira.cloud.kanbanalytics/cloud/overview
  [Epic Sum Up]: https://marketplace.atlassian.com/plugins/aptis.plugins.epicSumUp/cloud/overview
  [Tempo Planner for JIRA]: https://marketplace.atlassian.com/plugins/com.tempoplugin.tempo-planner/cloud/overview
  [JIRA Software Cloud tutorials]: /cloud/jira/software/learning
  [jira-agile-development tag]: https://answers.atlassian.com/questions/topics/753774/jira-agile-development
