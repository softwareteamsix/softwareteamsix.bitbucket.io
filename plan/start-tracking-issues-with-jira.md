---
title: Start tracking issues with JIRA Software
platform: cloud
product: integrate
category: plan
subcategory: track

date: "2017-01-09"
---
<!--- I'm going to add all the plan/track info to this page -->
# Start tracking issues with JIRA Software

<table class='aui'>
	<tr>
		<td><img src="public/cloud/images/Software-Agile.png"></td>
		<td>
			<h2>Agile best practices</h2>
			<p>If you're new to agile, we recommend using Scrum. Scrum involves time-boxing your work into sprints. With JIRA Software, your team can plan their work and keep track of their progress.</p>
			<p>To learn how to do Scrum with JIRA Software, check out our <a href="https://www.atlassian.com/agile/how-to-do-scrum-with-jira-software">Agile guide</a>.</p>
		</td>
	</tr>
	<tr>
		<td><img src="Track-Plan-FiniTrack-Planing-Tasks.png"></td>
		<td>
			<h2>Issues best practices</h2>
			<p>ssues are like tasks to be done - in a software project, they can represent things like bugs or features to be implemented. If you're wondering about which issue types to use, we recommend sticking to the following three types:</p>
			<ul>
				<li>Epics - A large piece of work that encompasses many issues</li>
				<li>Stories - A feature of your software that you're looking to implement, expressed from the perspective of the user</li>
				<li>Bugs - A problem that impairs product or service functionality</li>
			</ul>
			<p>Check out <a href="https://confluence.atlassian.com/jirasoftwarecloud/working-with-issues-764478424.html">Working with issues</a> for more information.</p>
	    </td>
	</tr>
	<tr>
		<td><img src="../images/Create-Share-Creating-Content.png"></td>
		<td>
			<h2>Requirements and meeting notes best practices - Confluence</h2>
			<p>Linking JIRA Software with Confluence allows you to create notes and documentation related to your project. Information related to your JIRA project will dynamically display on your page, so it's a useful way to keep track of your work. For example, you can use Confluence to:</p>
			<ul>
				<li>Define your requirements</li>
				<li>Manage your sprints</li>
				<li>Store your sprint retrospective notes</li>
				<li>Create JIRA issues directly from Confluence</li>
				<li>Show information from your JIRA Software project visually</li>
			</ul>
			<img src="../images/Confluence_JIRA_sprint_retro.png">
			<p>Check out Using <a href="https://confluence.atlassian.com/jirasoftwarecloud/using-jira-applications-with-confluence-776664835.html">JIRA applications with Confluence </a>for more information.</p>
		</td>
	</tr>
	<tr>
		<td><img src="../images/Create-Share-Big-News.png"></td>
		<td>
			<h2>Communication best practices - HipChat</h2>
			<p>Forget emails! HipChat is our communication tool that your team can use to send messages to each other. We've also got a mobile app, so your team can keep each other updated on-the-go. By linking JIRA Software with HipChat, you can:</p>
			<ul>
				<li>Connect your project to a HipChat room, so that basic information is displayed whenever an issue is mentioned
					<img src="../images/hipchat-autoconvert-preview.png"></li>
				<li>Create a HipChat room based on a JIRA Software issue, so you can keep all discussion related to a specific issue in one room.</li>
				<li>Set up HipChat notifications, so that when work is progressed, team members are notified.</li>
				<li>Get information on how many issues your team is working on at a glance</li>
			</ul>
			<p><img src="../images/software 4 @2x.png"> <img src="../images/software 11 @2x.png"></p>
			<p>Check out <a href="https://confluence.atlassian.com/jirasoftwarecloud/using-jira-applications-with-hipchat-776664836.html"></a>Using JIRA applications with HipChat for more info.</p>
		</td>
	</tr>
</table>
{{%feedback%}}
