---
title: Build branches often in Bitbucket
platform: cloud
product: integrate
category: ci
subcategory: continuousint

date: "2017-01-09"
---
# Build branches often in Bitbucket
