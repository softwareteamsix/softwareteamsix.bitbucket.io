---
title: Get branch build notifications in HipChat
platform: cloud
product: integrate
category: ci
subcategory: continuousint

date: "2017-02-17"
---
# Get branch build notifications in HipChat

If your team is using [HipChat](/cloud/integrate/plan/team-chat-is-cheap) to communicate, and we recommend that you use _some_ kind of real-time messaging instead of email, then you can easily get build status updates from [Bitbucket Pipelines](/cloud/integrate/develop/continuous-integration).

#screenshot#

Notifications in HipChat really helps the team to close the loop on continuous integration. The developer responsible for committing the updated branch gets almost instant feedback, and can quickly fix problems because the changes are fresh in their head. The team doesn't get held up by failing builds because builds are fixed quickly, but they also know immediately when there's a problem.

## Create a room in HipChat

If necessary, create a new room in HipChat to receive updates from Bitbucket. See []() if you're unsure how to do that.

If you're just gonna use an existing room, skip to the next section.

## Get build status updates

In Bitbucket, go to __Settings__ > __HipChat integrations__ for the repository.

Now, simply enter the room name, and check (or clear) the events that you want notifications for. Easy!
