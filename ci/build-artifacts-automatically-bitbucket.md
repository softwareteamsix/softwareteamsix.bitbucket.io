---
title: Build artifacts automatically in Bitbucket
platform: cloud
product: integrate
category: ci
subcategory: continuousint

date: "2017-01-09"
---
# Build artifacts automatically in Bitbucket
