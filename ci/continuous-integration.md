---
title: CI with Bitbucket Pipelines
platform: cloud
product: integrate
category: ci
subcategory: continuousint

date: "2017-02-20"
---
# CI with Bitbucket Pipelines

Bitbucket Pipelines is a continuous delivery (CI) tool baked right in to your Bitbucket repo.

It simplifies the management of your builds by keeping the build configuration in a single YAML file in the repository (named bitbucket-pipelines.yml).

It simplifies build environment and project dependency problems by encapsulating the build in a Docker image, which is specified by the YAML file.

You can use Bitbucket Pipelines for builds of any language for which a suitable Docker image is available.

Other cool things about Bitbucket Pipelines:

* Pipelines supports branch-based builds
* Pipelines sends build notifications by email, and to HipChat and Slack
* Pipelines can use pull requests to manage deployments, for example to Heroku or AWS
