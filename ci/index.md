---
title: Do CI with Atlassian
platform: cloud
product: integrate
category: ci
subcategory: index
date: "2017-02-17"
---
{{< reuse-page path="content/cloud/integrate/ci/do-ci-with-atlassian.md">}}
