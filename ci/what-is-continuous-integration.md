---
title: What do you mean by continuous integration
platform: cloud
product: integrate
category: ci
subcategory: continuousint

date: "2017-02-20"
---
# What do you mean by 'continuous integration'?

Continuous integration (CI) is a software development methodology that has:

* developers commit code changes to a central repository several times a day
* each commit automatically trigger a build
* the build tool check out the code from the repo
* the build tool compile the code and run the test suite
* problems immediately reported back to the team.

Variations in actual practice are related to considerations such as:

* how do you apply CI to branch-based development
* how do you manage project dependencies
* how do you manage environments for integration and functional testing

You can think of continuous integration and continuous deployment as the twin pillars of continuous delivery.
