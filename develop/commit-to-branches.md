---
title: Expect to commit to branches in Bitbucket
platform: cloud
product: integrate
category: develop
subcategory: isolation

date: "2017-01-09"
---
# Expect to commit to branches in Bitbucket

When you use Git branches...
