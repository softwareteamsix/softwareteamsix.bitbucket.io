---
title: Get pull request notifications in HipChat
platform: cloud
product: integrate
category: develop
subcategory: review

date: "2017-01-09"
---
# Get pull request notifications in HipChat
