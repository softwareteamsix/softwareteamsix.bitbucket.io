---
title: Avoid a branch-heavy workflow
platform: cloud
product: integrate
category: develop
subcategory: revision

date: "2017-01-09"
---
# Avoid a branch-heavy workflow

When you work on a development team, you may be touching similar parts of the code throughout a project. As a result, changes made in one part of the source can be incompatible with those made by another developer working at the same time. Version control, and more specifically Git, helps solve these types of problems.

[Learn more about version control](https://ru.atlassian.com/git/tutorials/what-is-version-control)

## Git basics

Git transfers code between your local system and Bitbucket. You also use Git to version your changes, revert back to previous versions, locate a specific change, and much more. Git is a distributed version control system, which means that every developer has the full history of their code repository locally. You can use Git from the command line or with a Git interface tool, like SourceTree. [Learn more](https://ru.atlassian.com/git/tutorials/what-is-git)

If you don't have any code in your repository, you'll start using Git as soon as you start coding. There's three parts to this process:

<ol>
	<li>
		<p><b>Clone your repository</b></p>
		<p>Cloning copies a version of the Bitbucket repository to your local system. In this case, nothing is in the Bitbucket repository, so cloning will add an empty folder locally and create a connection with Bitbucket that will allow you to push and pull when you do have code. <a href="https://confluence.atlassian.com/x/4whODQ">Learn more</a></p>
	</li>
	<li>
		<p><b>Work on local source files</b></p>
		<p>As you work, you want to add and commit your changes. When you make a commit, you'll enter a description of your changes. Make sure your description is clear and complete, as you may need to track down the change at some point in the future. <a href="https://confluence.atlassian.com/x/8QhODQ">Learn more</a></p>
	</li>
	<li>
		<p><b>Push updates to your repository</b></p>
		<p>To get your changes onto Bitbucket, you'll push your code to the repository so that other members can see them too. <a href="https://confluence.atlassian.com/x/NQ0zDQ">Learn more</a></p>
	</li>
</ol>

## Git feature branching

While Git is useful for saving and versioning your code individually, it's even more powerful when working on a team. We recommend the Git feature branch workflow for team development because of its efficient and straightforward path. In this workflow, all feature development takes place on branches separate from the main master branch. As a result, multiple developers can work on their own features without touching the main code.

<table>
<tbody>
	<tr>
		<td rowspan="8" width="35%" ><img src="/cloud/integrate/images/branch-diagram.png" height="1500px"  ></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
		<tr><td>
			<p><b>Start with the <code>master</code> branch</b></p>
			<p>This workflow helps you collaborate on your code with at least one other person. As long as your Bitbucket and local repos are up-to-date, you're ready to get started.</p></td>
		</tr>
		<tr><td>
			<p><b>Create a <code>new-branch</code></b><p>
			<p>Use a separate branch for each feature or issue you work on. After creating a branch, check it out locally so that any changes you make will be on that branch. <a href="https://confluence.atlassian.com/display/BITBUCKET/Branching+a+Repository">Learn more</a></p></td>
		</tr>
		<tr><td>
			<p><b>Update, add, commit, and push changes</b></p>
			<p>Work on the feature and make commits like you would any time you use Git. When ready, push your commits, updating the feature branch on Bitbucket. <a href="https://confluence.atlassian.com/display/BITBUCKET/Work+on+local+source+files">Learn more</a></p></td>
		</tr>
		<tr><td>
			<p><b>Get your code reviewed</b></p>
			<p>To get feedback on your code, create a pull request in Bitbucket. From there, you can add reviewers and make sure everything is good to go before merging. <a href="https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requests">Learn more</a></p></td>
		</tr>
		<tr><td>
			<p><b>Resolve feedback</b></p>
			<p>Now your teammates comment and approve. Resolve their comments locally, commit, and push changes to Bitbucket. Your updates appear in the pull request.</p>
				</td>
		</tr>
		<tr><td>
			<p><b>Merge your pull request</b></p>
			<p>Before you merge, you may have to resolve merge conflicts if others have made changes to the repo. When your pull request is approved and conflict-free, you can add your code to the master branch. Merge from the pull request in Bitbucket. <a href="https://confluence.atlassian.com/display/BITBUCKET/Resolve+merge+conflicts">Learn more</a></p></td>
		</tr>
</tbody>
</table>
{{% feedback %}}
