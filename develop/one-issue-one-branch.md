---
title: One JIRA issue, one Bitbucket branch
platform: cloud
product: integrate
category: develop
subcategory: isolation

date: "2017-01-09"
---
# One JIRA issue, one Bitbucket branch
