---
title: Develop with Atlassian
platform: cloud
product: integrate
category: develop
subcategory: index
date: "2017-01-09"
---
# Develop with Atlassian


Atlassian knows software development inside out!



## Track work to be done using JIRA Software Cloud

### Sign up to Atlassian Cloud

## Do version control with Git

{{< include path="content/cloud/integrate/develop/snippets/version-control.snippet.md">}}

<br>
Although there are many version control systems, Atlassian recommends, and uses, Git. We like Git because:

* It's a _distributed_ version control system (i.e. a DVCS), which makes it easy for our teams to use _feature branch workflows_
* It's popular, and so is widely understood and supported
* It allows for distributed development, so it's fast
* Repository hosting in Atlassian Bitbucket provides several advantages, including pull requests, access control and seamless integration with JIRA Software issue tracking.

You can read much more about Git at our [Getting Git Right](https://www.atlassian.com/git/) site.

If you're using another VCS, such as SVN, you can change to Git fairly easily. We think this is one of the first things you should do to help your team work more effectively. See our [migrate to Git tutorial](https://www.atlassian.com/git/tutorials/migrating-overview/) for guidance on this.

## Host source code in Bitbucket Cloud

### Get an account in Bitbucket

If you haven't already got an account, go to [https://bitbucket.org/account/signup/](https://bitbucket.org/account/signup/) and complete the sign up form.

Each of your team members need an individual account.

### Set up a Bitbucket team

A _team_ in Bitbucket let's you:
* set group permissions to control access to the team's repositories
* use projects to structure the team's repositories
* control costs by choosing the best Bitbucket pricing plan for the team

#### 1. Create a team

#### 2. Add people to the team

#### 3.

## Change isolation

## Get JIRA Software and Bitbucket syncing


## Code review



## Code coverage


## Set up continuous integration in Bitbucket


## Get notifications in HipChat
