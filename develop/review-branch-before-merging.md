---
title: Expect to review a branch before merging in Bitbucket
platform: cloud
product: integrate
category: develop
subcategory: review

date: "2017-01-09"
---
# Expect to review a branch before merging in Bitbucket
