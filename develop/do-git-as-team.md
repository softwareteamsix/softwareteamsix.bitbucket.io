---
title: Do Git as a team
platform: cloud
product: integrate
category: develop
subcategory: collaborate

date: "2017-01-13"
---
# Do Git as a team

Here's how your team can use Git to collaborate on a software project. We're assuming here that you've got some familiarity with Git, however you may want to read these [tutorials from Atlassian](https://www.atlassian.com/git/tutorials) first.

Where relevant we call out how the Atlassian dev tool stack can help streamline your dev process. If you've [got those tools integrated](/cloud/integrate/develop/develop-with-atlassian), you'll be able to:

* Manage your project, and track work, using JIRA Software Cloud
* Host your project's source code in Bitbucket Cloud
* Collaborate on code reviews using pull requests in Bitbucket Cloud
* Set up continuous integration and deployment with Bitbucket Pipelines
* Get notifications in HipChat on the status of each step of your process, including commits, pull requests and build results.



## Do Git with the command line or with SourceTree

Devs in your team can do Git with either the command line or the SourceTree client, whichever they're most comfortable with. To get up to speed with basic Git procedures with those, see:

* @todo command line tutorial link
* [Do Git with SourceTree](/cloud/integrate/develop/do-git-with-sourcetree)


## Create a branch for each unit of work

If you're using JIRA Software to track work to be done for your project, and you should be using _something_, you'll typically use a single issue for each unit of work. Further, the code changes for that unit of work are best isolated in a single branch of the repository. If you've been reading the Atlassian Git tutorials, we're following the [Feature Branch workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow) here, but you can use others.

So, before anyone begins coding against that issue, there needs to be a new Git branch in the repo. The easiest way to create that is for a dev to simply click __Create branch__ in the 'Development' panel when they pick up the issue in JIRA Software. You'll notice that the new branch name uses the JIRA issue key for easy and consistent identification.

@todo - transition the issue

## Clone and work locally

Each dev working on the issue should clone the repo locally and check out the new branch:

    git clone git@bitbucket.org:path/to/repo.git
    git checkout PROJ-123

Note that we used the SSH protocol to clone the repo here.

As you work, you should regularly (like every day, but preferably more often) commit to the branch:

    git add -a
    git commit -m "PROJ-123 oauth tests"

By including the issue key in the commit message you enable JIRA Software and Bitbucket to sync, so work on the issue is visible in JIRA (once you've pushed your changes to Bitbucket). Note that the issue key must use the default JIRA key format – two or more uppercase letters followed by a hyphen and the issue number - for example, ABC-123.

@todo - does rebase fit here?

## Push to Bitbucket for continuous integration

When you push to the branch in the central repo (in Bitbucket) Bitbucket Pipelines gets triggered to build that branch and run your CI tests. You'll get build notifications in HipChat.

@todo - build notifications in HipChat

## Open a pull request to review changes

When ready, create a pull request in BB, to discuss merging the branch to master.

merge

@todo - transition the issue to closed

Once your work on this issue is completed, you may want to consider deploying the new feature...
