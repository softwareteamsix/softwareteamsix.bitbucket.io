Version control, also known as 'revision control' or 'source control', is the management of changes in your project.
Version control is fundamental to a software development project, and becomes critically important as your team grows or becomes globally distributed.
